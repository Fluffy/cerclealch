from PIL import Image, ImageDraw
from math import *
from random import randint
import os, sys

def cercle (im, Xcentre, Ycentre, rayon):
    pinceau = ImageDraw.Draw(im, mode=None)
    pinceau.arc((Xcentre-rayon, Ycentre-rayon, Xcentre+rayon, Ycentre+rayon),0,360,"black")

def cercleOpaque (im, Xcentre, Ycentre, rayon):
    pinceau = ImageDraw.Draw(im, mode=None)
    pinceau.chord((Xcentre-rayon, Ycentre-rayon, Xcentre+rayon, Ycentre+rayon),0,360,"white", "black")


def carre (im, Xcentre, Ycentre, rayon):
    pinceau = ImageDraw.Draw(im, mode=None)
    pinceau.polygon([(Xcentre-rayon, Ycentre),(Xcentre, Ycentre+rayon),(Xcentre+rayon, Ycentre),(Xcentre, Ycentre-rayon)],None,"red")
    return floor(rayon*cos(pi/4))

def polygon (im, Xcentre, Ycentre, rayon, nbreCote, angle):
    pinceau = ImageDraw.Draw(im, mode=None)
    for i in range (nbreCote):
        X0 = Xcentre + rayon * cos(2*pi*i/nbreCote+angle)
        Y0 = Ycentre + rayon * sin(2*pi*i/nbreCote+angle)
        X1 = Xcentre + rayon * cos(2*pi*(i+1)/nbreCote+angle)
        Y1 = Ycentre + rayon * sin(2*pi*(i+1)/nbreCote+angle)
        pinceau.line([(X0,Y0),(X1,Y1)], "red" )
    return floor(rayon*cos(pi/nbreCote))

def etoile(im, Xcentre, Ycentre, rayon, nbreCote, angle, saut, minicercle):
    pinceau = ImageDraw.Draw(im, mode=None)
    for i in range (nbreCote):
        X0 = Xcentre + rayon * cos(2*pi*i/nbreCote+angle)
        Y0 = Ycentre + rayon * sin(2*pi*i/nbreCote+angle)
        X1 = Xcentre + rayon * cos(2*pi*(i+saut)/nbreCote+angle)
        Y1 = Ycentre + rayon * sin(2*pi*(i+saut)/nbreCote+angle)
        pinceau.line([(X0,Y0),(X1,Y1)], "red" )
        if(minicercle):
            miniCercle(im, X0, Y0, rayon / 12, angleBase)
    return floor(rayon*abs(cos(saut*pi/nbreCote)))

def etoilePoly(im, Xcentre, Ycentre, rayon, nbreCote, angle, saut, minicercle):
    polygon(im, Xcentre, Ycentre, rayon, nbreCote, angle)
    return etoile(im, Xcentre, Ycentre, rayon, nbreCote, angle, saut, minicercle)

def miniCercle(im, Xcentre, Ycentre, rayon, angleBase):
    cercleOpaque(im, Xcentre,Ycentre,rayon)
    for i in range(3):
        rayon=nextStep(im, Xcentre,Ycentre, rayon, angleBase, False)


def nextStep(im, Xcentre, Ycentre, rayon, angleBase, minicercle):
    cercle(im, Xcentre,Ycentre,rayon)
    nbreCote =  randint(5,12)
    saut = randint(2,min(nbreCote-1, 3))
    if(nbreCote % saut == 0):
        saut = 1
    rayon = etoilePoly(im, Xcentre,Ycentre, rayon, nbreCote,angleBase,saut, minicercle)
    return rayon



im = Image.new ("RGBA",(800,800), "white")
angleBase = -pi/2
Xcentre = 400
Ycentre = 400
rayon = 400

for i in range(4):
    rayon = nextStep(im, Xcentre, Ycentre, rayon, angleBase, True)


"""
cercle(im, 400,400,400)
rayon = polygon(im, 400,400,400,5,angle)
cercle(im, 400,400,rayon)
rayon = polygon(im, 400,400,rayon,7,angle)
cercle(im, 400,400,rayon)
rayon = polygon(im, 400,400,rayon,6,angle)
cercle(im, 400,400,rayon)
"""

"""
pinceau = ImageDraw.Draw(im, mode=None)
pinceau.arc((10,10,410,410), 0, 360, fill=128, width=2)
#pinceau.chord((400,10,800,400), 0, -180, fill="black", outline="red")
pinceau.line((100,40,10,210), fill="green", width = 10)
"""


im.save("test.png", "PNG")

im.show()
